import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { userLocalService } from "../../services/localService";

export default function UserNav() {
    let user = useSelector((state) => {
        return state.userReducer.user;
    });
    console.log("user: ", user);
    const handleLogOut = () => {
        userLocalService.remove();
        // window.location.href = "/login";
        window.location.reload();
    };
    const renderContent = () => {
        if (user) {
            return (
                <>
                    <span>{user?.hoTen}</span>
                    <button
                        onClick={handleLogOut}
                        className="border border-black px-5 py-2 rounded"
                    >
                        Đăng Xuất
                    </button>
                </>
            );
        } else {
            return (
                <>
                    <NavLink to={"/login"}>
                        <button className="border border-black px-5 py-2  rounded">
                            Đăng Nhập
                        </button>
                    </NavLink>
                    <button className="border border-black px-5 py-2 rounded">
                        Đăng Ký
                    </button>
                </>
            );
        }
    };

    return <div className="space-x-3">{renderContent()}</div>;
}
