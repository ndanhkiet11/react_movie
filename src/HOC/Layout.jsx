import React from "react";
import Footer from "../Components/Footer/Footer";
import Header from "../Components/Header/Header";

export default function Layout({ children }) {
    // children là các component hay nội dung được lồng ở trong các component
    return (
        <div>
            <Header />
            {children}
            <Footer />
        </div>
    );
}
