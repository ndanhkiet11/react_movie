import React from "react";
import { Button, Form, Input, message } from "antd";
import { postLogin } from "../../services/userService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { userLocalService } from "../../services/localService";
import Lottie from "lottie-react";
import bg_animate from "../../assets/90315-christmas-tree.json";
import {
    setUserAction,
    setUserActionService,
} from "../../redux/action/userAction";
export default function LoginPage() {
    let navigate = useNavigate();
    let dispatch = useDispatch();
    const onFinish = (values) => {
        console.log("Success:", values);
        postLogin(values)
            .then((res) => {
                console.log(res);
                message.success("Đăng nhập thành công!");
                dispatch(setUserAction(res.data.content));
                userLocalService.set(res.data.content);
                setTimeout(() => {
                    // window.location.href = "/"; ko dùng vì nó sẽ load lại trang

                    navigate("/");
                }, 1000);
            })
            .catch((err) => {
                message.error("Đăng nhập thất bại!");
                console.log(err);
            });
    };
    const onFinishReduxThunk = (value) => {
        const handleNavigate = () => {
            setTimeout(() => {
                // window.location.href = "/"; ko dùng vì nó sẽ load lại trang

                navigate("/");
            }, 1000);
        };
        dispatch(setUserActionService(value, handleNavigate));
    };
    const onFinishFailed = (errorInfo) => {
        console.log("Failed:", errorInfo);
    };
    return (
        <div className="w-screen p-5 h-screen  justify-center items-center bg-blue-500 ">
            <div className="container p-5 bg-white rounded flex ">
                <div className="w-1/2 ">
                    <Lottie animationData={bg_animate} loop={true} />
                </div>

                <div className="w-2/3   flex flex-col justify-center items-center">
                    <Form
                        className="w-2/3"
                        layout="vertical"
                        name="basic"
                        labelCol={{
                            span: 12,
                        }}
                        wrapperCol={{
                            span: 24,
                        }}
                        initialValues={{
                            remember: true,
                        }}
                        onFinish={onFinishReduxThunk}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                    >
                        <Form.Item
                            label="Username"
                            name="taiKhoan"
                            rules={[
                                {
                                    required: true,
                                    message: "Please input your username!",
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Password"
                            name="matKhau"
                            rules={[
                                {
                                    required: true,
                                    message: "Please input your password!",
                                },
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item
                            wrapperCol={{
                                span: 24,
                            }}
                            className="text-center"
                        >
                            <Button htmlType="submit">Submit</Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </div>
    );
}
