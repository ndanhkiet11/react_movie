import React from "react";
import { useParams } from "react-router-dom";

export default function DetailPage() {
    let params = useParams();
    console.log("params: ", params);
    return (
        <div>
            <h2 className="text-red-500 font-black text-center">
                Ma Phim: {params.id}
            </h2>
        </div>
    );
}
