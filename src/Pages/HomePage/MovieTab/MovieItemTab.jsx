import moment from "moment/moment";
import "moment/locale/vi";
import React from "react";
moment.locale("vi");

export default function MovieItemTab({ movie }) {
    return (
        <div className="flex mt-10 space-x-5">
            <img
                className="h-40 w-28 object-cover"
                src={movie.hinhAnh}
                alt=""
            />
            <div>
                <h3 className="font-medium text-2xl">{movie.tenPhim}</h3>
                <div className="grid grid-cols-3 gap-7 mt-5">
                    {movie.lstLichChieuTheoPhim.slice(0, 6).map((lichChieu) => {
                        return (
                            <span className="bg-red-500 text-white px-3 py-2 rounded">
                                {moment(lichChieu.ngayChieuGioChieu).format(
                                    "DD/MM hh:mm A"
                                )}
                            </span>
                        );
                    })}
                </div>
            </div>
        </div>
    );
}

/**
 * {
    "lstLichChieuTheoPhim": [
        {
            "maLichChieu": 44436,
            "maRap": "550",
            "tenRap": "Rạp 10",
            "ngayChieuGioChieu": "2021-10-16T07:23:28",
            "giaVe": 75000
        },
        {
            "maLichChieu": 44437,
            "maRap": "547",
            "tenRap": "Rạp 7",
            "ngayChieuGioChieu": "2021-10-16T09:23:51",
            "giaVe": 75000
        },
        {
            "maLichChieu": 44438,
            "maRap": "547",
            "tenRap": "Rạp 7",
            "ngayChieuGioChieu": "2021-10-17T02:23:57",
            "giaVe": 90000
        }
    ],
    "maPhim": 8395,
    "tenPhim": "Băng cướp thế kỷ: Đẳng cấp quý cô",
    "hinhAnh": "https://movienew.cybersoft.edu.vn/hinhanh/bang-cuop-the-ky-dang-cap-quy-co_gp01.jpg",
    "hot": true,
    "dangChieu": false,
    "sapChieu": true
}
 */
