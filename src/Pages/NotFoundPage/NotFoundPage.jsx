import React from "react";

export default function NotFoundPage() {
    return (
        <div className="h-screen w-screen flex justify-center items-center">
            <h1 className="text-red-700  text-5xl animate-ping">
                404 NOT FOUND
            </h1>
        </div>
    );
}
