import { message } from "antd";
import { postLogin } from "../../services/userService";
import { SET_USER_LOGIN } from "../constant/userConstant";

export const setUserAction = (value) => {
    return {
        type: SET_USER_LOGIN,
        payload: value,
    };
};

export const setUserActionService = (value, onSuccess) => {
    return (dispatch) => {
        postLogin(value)
            .then((res) => {
                message.success("Đăng nhập thành công!");
                dispatch({
                    type: SET_USER_LOGIN,
                    payload: res.data.content,
                });
                onSuccess();
            })
            .catch((err) => {
                console.log(err);
            });
    };
};
